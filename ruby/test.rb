load 'ThousandsSeparator.rb'

form = ThousandsSeparator.new()
#string como input
entrada = "1234"
saida = "1,234"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
end

# string como input
entrada = "123456789"
saida = "123,456,789"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
end

#inteiro como input
entrada = 1234
saida = "1,234"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
end

#nil como input
entrada = nil
saida = "0"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
end

#String inválida
entrada = "1a23456789"
saida = "0"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
end

entrada = "-1234567"
saida = "-1,234,567"
if form.format(entrada) == saida
    puts "sucesso!" 
else 
    puts "Falha!"
    puts form.format(entrada)
end

#Outro delimitador e valor negativo
entrada = "-1234567"
saida = "-1.234.567"
if form.format(entrada,".") == saida
    puts "sucesso!" 
else 
    puts "Falha!"
    puts form.format(entrada)
end
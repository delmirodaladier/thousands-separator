class ThousandsSeparator
    #metodo público
    def format(entrada, delimitador=nil)
        if delimitador.nil?
          delimitador = ","
        end
        #Verifica se o tipo é Inteiro ou String, caso contrário retorna 0
        if !entrada.is_a?Integer and !entrada.is_a?String
          return "0"
        end
        #transforma de inteiro para string
        if entrada.is_a? Integer
          entrada = entrada.to_s
        end
        #Verifica se a string é numérica
        if !entrada.match('^-?[0-9]+$')
          return '0'
        end

        #variável de saída
        out = ""
        
        a = entrada.reverse.split(/(...)/).reject(&:empty?).map(&:reverse)
        for i in 0..a.length-2
          out = delimitador+a[i].to_s+ out 
        end
        out = a.last.to_s + out
        return out
      end
end
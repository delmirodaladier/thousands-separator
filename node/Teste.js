var ThousandsSeparatorRegExp = require('./ThousandsSeparatorRegExp.js');

//Entrada comum
console.log(ThousandsSeparatorRegExp.format('10000000000'));
//Entrada Inteira
console.log(ThousandsSeparatorRegExp.format(10000000000));
//Entrada negativa
console.log(ThousandsSeparatorRegExp.format('-10000000000'));
//Entrada negativa inteira
console.log(ThousandsSeparatorRegExp.format(10000000000));
//Ponto como delimitador
console.log(ThousandsSeparatorRegExp.format('10000000000','.'));
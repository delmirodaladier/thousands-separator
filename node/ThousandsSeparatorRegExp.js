
class ThousandsSeparatorRegExp {

    static format(input, delimiter=','){
        if (delimiter.length > 1){
            throw "delimiter not single character";
        }
        if (input && !alfanumerico(input)){
            let valor  = String(input);                             // converte valor para string
            let reverse = reverseString(valor);                     // inverte a string
            let patt =/([\d]{1,3})(\-)*/g;                          // regExp separador de milhar
            let arr = reverse.match(patt);                          // busca por matches
            return reverseString(joinArray(arr, delimiter));        // retorna desinvertidos
        } else 
            return '0';
    }
}

function alfanumerico (str){
    let patt = /(^-)([\d]*)([\D]+)/g;
    return patt.test(str);  
}

function reverseString(str) {
    return str.split("").reverse().join("");
}

function joinArray(array, delimiter) {
    return array.join(delimiter);
}

module.exports = ThousandsSeparatorRegExp;